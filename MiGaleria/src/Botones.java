//00000000
import entities.Album;
import entities.Imagen;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javafx.scene.image.Image;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Municipio de Gye
 */
public class Botones {
    public ArrayList<Imagen> FiltroNombre(HashMap<Album,ArrayList<Imagen>> galeria, Album album, String nombre){
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            if (img.getNombre()==nombre){
                filImagen.add(img);
            }
        }
        return filImagen;     
    }
    public ArrayList<Imagen> FiltroLugar(HashMap<Album,ArrayList<Imagen>> galeria, Album album, String lugar){
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            if (img.getLugar()==lugar){
                filImagen.add(img);
            }
        }
        return filImagen;     
    }
    public ArrayList<Imagen> FiltroFecha(HashMap<Album,ArrayList<Imagen>> galeria, Album album, String fecha){
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            if (img.getFecha()==fecha){
                filImagen.add(img);
            }
        }
        return filImagen;     
    }
    public ArrayList<Imagen> FiltroEtiqueta(HashMap<Album,ArrayList<Imagen>> galeria, Album album, String etiqueta){
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            if (img.getEtiqueta()==etiqueta){
                filImagen.add(img);
            }
        }
        return filImagen;     
    }
    public ArrayList<Imagen> FiltroReaccion(HashMap<Album,ArrayList<Imagen>> galeria, Album album, String reaccion){
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            if (img.getReaccion()==reaccion){
                filImagen.add(img);
            }
        }
        return filImagen;     
    }
    public ArrayList<Imagen> FiltroCamara(HashMap<Album,ArrayList<Imagen>> galeria, Album album, String camara){
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            if (img.getCamara()==camara){
                filImagen.add(img);
            }
        }
        return filImagen;     
    }
    public Imagen Reaccionando(Imagen img, String reaccion){
        img.setReaccion(reaccion);
        return img;
    }
    
    
    
    
}




