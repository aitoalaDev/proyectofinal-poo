/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galeria2;

import MetodosVarios.Botones;
import MetodosVarios.SlideShow;
import entities.Imagen;
import entities.Album;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
//000000000000000000000000000000000000000000000
/**
 *
 * @author Carlos
 */
public class Galeria2 extends Application implements Serializable {
    static HashMap<Album, ArrayList<Imagen>> albumes=new HashMap<>();
    static String busqueda="Que Buscas: ";
    static int contador=0;
    static int contador2=0;
    @Override
    public void start(Stage stage) {
        ComboBox albumSelect=new ComboBox();  
        
        HBox panel=new HBox(20);
        Button añadir=new Button("añadir imagen");
        Button crearAlbum=new Button("Crear album");
        Button filtros=new Button("Filtros");
        Button borrar=new Button("Eliminar");
        Button mover=new Button("Mover fotos");
        Button slideShow=new Button("Ver las Imagenes");
        TextField tiempo=new TextField();
        
        BorderPane root = new BorderPane(); 
        root.setTop(new Label("Imagenes de la Galeria:"));
        root.setBottom(panel);
        panel.getChildren().add(albumSelect);
        panel.getChildren().add(añadir);
        panel.getChildren().add(crearAlbum);
        panel.getChildren().add(borrar);
        panel.getChildren().add(mover);
        panel.getChildren().add(slideShow);
        panel.getChildren().add(tiempo);
        ScrollPane desplazador=new ScrollPane();
        VBox caja=new VBox();
        TilePane tilePane = new TilePane();
        tilePane.setAlignment(Pos.CENTER);
        tilePane.setPadding(new Insets(15, 15, 15, 15));
        tilePane.setVgap(30);
        tilePane.setHgap(20);
        desplazador.setContent(tilePane);
        caja.getChildren().add(desplazador);
        root.setLeft(caja);
        panel.getChildren().add(filtros);
        
        cargar(albumSelect,desplazador,root);
        actualizar(albumSelect,desplazador,root);
        
        stage.setOnCloseRequest((g)->{
            guardar(albumSelect,desplazador,root);
        });
        
        mover.setOnAction((dfs)->{
            Stage moviendo=new Stage();
            ComboBox pick=new ComboBox();
            GridPane paneMover=new GridPane();
            Button aceptar=new Button("Aceptar");
            ComboBox select=new ComboBox();
            
            for(Album token:albumes.keySet()){
                select.getItems().add((String)token.getNombre());
                if(token.getNombre().equals(albumSelect.getValue())){
                    for(Imagen img:albumes.get(token)){
                        pick.getItems().add((String)img.getNombre());
                        
                                    
                    }
                }
            }
            aceptar.setOnAction((g)->{
                ArrayList<Imagen> listaImagen=new ArrayList();
                ArrayList<Album> listaAlbum=new ArrayList();
                for(Album token:albumes.keySet()){
                    if(token.getNombre().equals((String)albumSelect.getValue())){
                        listaAlbum.add(token);
                        for(Imagen img:albumes.get(token)){
                            if(img.getNombre().equals(pick.getValue())){
                                listaImagen.add(img);
                            }
                        }
                    }
                    if(token.getNombre().equals((String)select.getValue())){
                        listaAlbum.add(token);
                    }    
                }
                albumes.get(listaAlbum.get(0)).removeAll(listaImagen);
                actualizarLlave(listaAlbum.get(1),listaImagen.get(0));
                actualizar(albumSelect,desplazador,root);
                moviendo.close();
            });

            
            paneMover.add(new Label("¿Que foto desea mover? "), 0, 0);
            paneMover.add(pick, 0, 1);
            paneMover.add(new Label(" mover a: "), 1, 1);
            paneMover.add(select, 2, 1);
            paneMover.add(aceptar, 3, 1);
            Scene escena1=new Scene(paneMover);                
            moviendo.setScene(escena1);
            moviendo.show();
            
        });
        
        borrar.setOnAction((f)->{
            Stage eliminacion=new Stage();
            ComboBox pick=new ComboBox();
            GridPane paneEliminar=new GridPane();
            Button photoDelete=new Button("Aceptar");
            Button albumDelete=new Button("Eliminar este album");
            for (Album token:albumes.keySet()){
                if(token.getNombre().equals(albumSelect.getValue()))
                    for(Imagen img:albumes.get(token)){
                        pick.getItems().add((String)img.getNombre());
                    }
            }
            
            albumDelete.setOnAction((cx)->{
                ArrayList<Album> found = new ArrayList<>();
                for (Album token:albumes.keySet()){
                    if(token.getNombre().equals(albumSelect.getValue())){
                        found.add(token);
                    }
                }
                albumes.remove(found.get(0));
                String album=found.get(0).getNombre();
                albumSelect.getItems().remove(album);
                actualizar(albumSelect,desplazador,root);
                eliminacion.close();
            });
            
            photoDelete.setOnAction((v)->{
                ArrayList<Imagen> found = new ArrayList<>();
                for (Album token:albumes.keySet()){
                    if(token.getNombre().equals(albumSelect.getValue())){
                        for(Imagen img:albumes.get(token)){
                            if(img.getNombre().equals((String)pick.getValue())){
                                found.add(img);
                            }
                        }
                        albumes.get(token).removeAll(found);
                    }
                }
                actualizar(albumSelect,desplazador,root);
                eliminacion.close();
            });
            
            paneEliminar.add(new Label("¿Que desea eliminar? "), 0, 0);
            paneEliminar.add(pick, 0, 1);
            paneEliminar.add(photoDelete, 1, 1);
            paneEliminar.add(albumDelete, 1, 2);
            Scene escena1=new Scene(paneEliminar);                
            eliminacion.setScene(escena1);
            eliminacion.show();
        });
        
        
        filtros.setOnAction((value)->{
            Stage filtroP=new Stage();
            VBox cajaP=new VBox();
            Button fNombre=new Button("Nombre");
            Button fLugar=new Button("Lugar");
            Button fFecha=new Button("Fecha");
            Button fCamara=new Button("Camara");
            Button fEtiqueta=new Button("Hashtag");
            Button fReaccion=new Button("Reaccion");
            cajaP.getChildren().addAll(fNombre,fLugar,fFecha,fCamara,fEtiqueta,fReaccion);
            GridPane opciones=new GridPane();
            opciones.add(fNombre, 0, 0);
            opciones.add(fLugar, 0, 1);
            opciones.add(fFecha, 0, 2);
            opciones.add(fCamara, 1, 0);
            opciones.add(fEtiqueta, 1, 1);
            opciones.add(fReaccion, 1, 2);
            opciones.setPrefSize(10, 10);
            opciones.setPadding(new Insets (15));
            cajaP.getChildren().add(opciones);
            Scene filtroS=new Scene(cajaP,170,125);
            
            filtroP.setScene(filtroS);
            filtroP.setTitle("Filtros");
            filtroP.show();
            
            fNombre.setOnAction((a)->{
                Stage b=new Stage();
                VBox caja1=new VBox();
                String busqueda="Que Buscas: ";
                Label info=new Label(busqueda);
                TextField entrada=new TextField();
                GridPane opciones1=new GridPane();
                opciones1.add(info, 0, 0);
                opciones1.add(entrada, 1, 0);
                
                
                    entrada.setOnKeyPressed(e->{
                        if (e.getCode() == KeyCode.ENTER) {
                            for(Album token: albumes.keySet()){                   
                                if(token.getNombre()==albumSelect.getValue()){
                                    ArrayList<Imagen> array=Botones.FiltroNombre(albumes, token, (String)entrada.getText());
                                    actualizarFiltro(array,albumSelect,desplazador,root);
                                    b.close();
                                    filtroP.close();
                                }
                            }
                        
                        }
                
                });
                    
                
                
                
                caja1.getChildren().add(opciones1);
                caja1.setPadding(new Insets(15));
                b.setTitle(busqueda);
                Scene escena1=new Scene(caja1);                
                b.setScene(escena1);
                b.show();
            });
            
            fLugar.setOnAction((a)->{
                
                Stage b=new Stage();
                VBox caja1=new VBox();
                Label info=new Label(busqueda);
                TextField entrada=new TextField();
                GridPane opciones1=new GridPane();
                opciones1.add(info, 0, 0);
                opciones1.add(entrada, 1, 0);
                
                entrada.setOnKeyPressed(e->{
                        if (e.getCode() == KeyCode.ENTER) {
                            for(Album token: albumes.keySet()){                   
                                if(token.getNombre()==albumSelect.getValue()){
                                    ArrayList<Imagen> array=Botones.FiltroLugar(albumes, token, (String)entrada.getText());
                                    actualizarFiltro(array,albumSelect,desplazador,root);
                                    b.close();
                                    filtroP.close();
                                }
                            }
                        
                        }
                
                });
                
                caja1.getChildren().add(opciones1);
                caja1.setPadding(new Insets(15));
                b.setTitle(busqueda);
                Scene escena1=new Scene(caja1);
                b.setScene(escena1);
                b.show();
            });
            
            fFecha.setOnAction((a)->{
                Stage b=new Stage();
                VBox caja1=new VBox();
                Label info=new Label(busqueda);
                TextField entrada=new TextField();
                GridPane opciones1=new GridPane();
                opciones1.add(info, 0, 0);
                opciones1.add(entrada, 1, 0);
                
                entrada.setOnKeyPressed(e->{
                        if (e.getCode() == KeyCode.ENTER) {
                            for(Album token: albumes.keySet()){                   
                                if(token.getNombre()==albumSelect.getValue()){
                                    ArrayList<Imagen> array=Botones.FiltroFecha(albumes, token, (String)entrada.getText());
                                    actualizarFiltro(array,albumSelect,desplazador,root);
                                    b.close();
                                    filtroP.close();
                                }
                            }
                        
                        }
                
                });
                
                caja1.getChildren().add(opciones1);
                caja1.setPadding(new Insets(15));
                b.setTitle(busqueda);
                Scene escena1=new Scene(caja1);
                b.setScene(escena1);
                b.show();
            });
            
            fCamara.setOnAction((a)->{
                Stage b=new Stage();
                VBox caja1=new VBox();
                Label info=new Label(busqueda);
                TextField entrada=new TextField();
                GridPane opciones1=new GridPane();
                opciones1.add(info, 0, 0);
                opciones1.add(entrada, 1, 0);
                
                entrada.setOnKeyPressed(e->{
                        if (e.getCode() == KeyCode.ENTER) {
                            for(Album token: albumes.keySet()){                   
                                if(token.getNombre()==albumSelect.getValue()){
                                    ArrayList<Imagen> array=Botones.FiltroCamara(albumes, token, (String)entrada.getText());
                                    actualizarFiltro(array,albumSelect,desplazador,root);
                                    b.close();
                                    filtroP.close();
                                }
                            }
                        
                        }
                
                });
                
                caja1.getChildren().add(opciones1);
                caja1.setPadding(new Insets(15));
                b.setTitle(busqueda);
                Scene escena1=new Scene(caja1);
                b.setScene(escena1);
                b.show();
            });
            
            fEtiqueta.setOnAction((a)->{
                Stage b=new Stage();
                VBox caja1=new VBox();
                Label info=new Label(busqueda);
                TextField entrada=new TextField();
                GridPane opciones1=new GridPane();
                opciones1.add(info, 0, 0);
                opciones1.add(entrada, 1, 0);
                
                entrada.setOnKeyPressed(e->{
                        if (e.getCode() == KeyCode.ENTER) {
                            for(Album token: albumes.keySet()){                   
                                if(token.getNombre()==albumSelect.getValue()){
                                    ArrayList<Imagen> array=Botones.FiltroEtiqueta(albumes, token, (String)entrada.getText());
                                    actualizarFiltro(array,albumSelect,desplazador,root);
                                    b.close();
                                    filtroP.close();
                                }
                            }
                        
                        }
                
                });
                
                caja1.getChildren().add(opciones1);
                caja1.setPadding(new Insets(15));
                b.setTitle(busqueda);
                Scene escena1=new Scene(caja1);
                b.setScene(escena1);
                b.show();
            });
            fReaccion.setOnAction((a)->{
                Stage b=new Stage();
                VBox caja1=new VBox();
                Label info=new Label(busqueda);
                TextField entrada=new TextField();
                GridPane opciones1=new GridPane();
                opciones1.add(info, 0, 0);
                opciones1.add(entrada, 1, 0);
                
                entrada.setOnKeyPressed(e->{
                        if (e.getCode() == KeyCode.ENTER) {
                            for(Album token: albumes.keySet()){                   
                                if(token.getNombre()==albumSelect.getValue()){
                                    ArrayList<Imagen> array=Botones.FiltroFecha(albumes, token, (String)entrada.getText());
                                    actualizarFiltro(array,albumSelect,desplazador,root);
                                    b.close();
                                    filtroP.close();
                                }
                            }
                        
                        }
                
                });
                
                caja1.getChildren().add(opciones1);
                caja1.setPadding(new Insets(15));
                b.setTitle(busqueda);
                Scene escena1=new Scene(caja1);
                b.setScene(escena1);
                b.show();
            });
        });
        
        
        crearAlbum.setOnAction((evento)->{
            Button newAlbumOk=new Button("OK");
            Stage newAlbum=new Stage();
            HBox newAlbumBox=new HBox();
            TextField newAlbumName=new TextField();
            newAlbumBox.setPadding(new Insets(15));
            newAlbumBox.getChildren().add(new Label("Ingrese el nombre del album: "));
            newAlbumBox.getChildren().add(newAlbumName);
            newAlbumBox.getChildren().add(newAlbumOk);
            newAlbumOk.setOnAction((newAlbumEvent)->{
                Album album=new Album(newAlbumName.getText());
                ArrayList<Imagen> array=new ArrayList<Imagen>();
                albumes.put(album, array);
                albumSelect.getItems().add(album.getNombre());
                newAlbum.close();
            });
            
            Scene newAlbumScene=new Scene(newAlbumBox,375,50);
            newAlbum.setTitle("Album Nuevo");
            newAlbum.setScene(newAlbumScene);
            newAlbum.show();
        });
        
        
        slideShow.setOnAction(event->{
            contador=0;
            contador2=0;
            Stage mostrador=new Stage();
            BorderPane vistaImagen=new BorderPane();
            GridPane opciones=new GridPane();
            ScrollPane invocador=new ScrollPane();
            vistaImagen.setCenter(invocador);
            Timer chrono=new Timer();
            ArrayList<ImageView> array=new ArrayList();
            VBox aux=(VBox)desplazador.getContent();
            for(Node component:aux.getChildren()){
                ImageView ivAux=(ImageView)component;
                ivAux.setFitHeight(400);
                ivAux.setFitWidth(700);
                array.add(ivAux);
            }
            TimerTask trigger=new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(()->{
                        contador++;
                        if (contador > array.size()) {
                            chrono.cancel();
                            chrono.purge();
                            actualizar(albumSelect,desplazador,root);
                            mostrador.close();
                            }
                        invocador.setContent(array.get(contador2));
                        contador2++;
                    });
                }
            };
            
            chrono.schedule(trigger, 1000, Long.parseLong(tiempo.getText())*1000);
            
            opciones.setHgap(4);
            vistaImagen.setBottom(opciones);
            Scene vistaGeneral=new Scene(vistaImagen,1000,700);
            mostrador.setScene(vistaGeneral);
            mostrador.setTitle("Vista en SlideShow");
            mostrador.show();    
        });
        
        añadir.setOnAction((value)->{
           Stage add=new Stage();
            GridPane addPane=new GridPane();
            Label addNombre=new Label("Nombre: ");
            Label addPersonas=new Label("Personas: ");
            Label addLugar=new Label("Lugar: ");
            Label addFecha=new Label("Fecha: ");
            Label addDescripcion=new Label("Descripcion: ");
            Label addImagen=new Label("Imagen: ");
            Label addCamara=new Label("Camara: ");
            Label addEtiqueta=new Label("Etiqueta: ");
            
            TextField newNombre=new TextField();
            TextField newPersonas=new TextField();
            TextField newLugar=new TextField();
            TextField newFecha=new TextField();
            TextField newDescripcion=new TextField();
             TextField newCamara=new TextField();
            TextField newEtiqueta=new TextField();
            ComboBox<String> newReaccion=new ComboBox();
            
            newReaccion.getItems().add("Like :v");
            newReaccion.getItems().add("Love it <3");
            newReaccion.getItems().add("Hahaha xD");
            newReaccion.getItems().add("Wowser :O");
            newReaccion.getItems().add("Sad :'v");
            newReaccion.getItems().add("Angry >:v");
            
            addPane.add(addNombre, 0, 0);
            addPane.add(addPersonas, 0, 1);
            addPane.add(addLugar, 0, 2);
            addPane.add(addFecha, 0, 3);
            addPane.add(addDescripcion, 0, 4);
            addPane.add(addImagen, 0, 8);
            addPane.add(addCamara, 0, 5);
            addPane.add(addEtiqueta, 0, 6);
            addPane.add(newReaccion,1,7);
            
            addPane.add(newNombre, 1, 0);
            addPane.add(newPersonas, 1, 1);
            addPane.add(newLugar, 1, 2);
            addPane.add(newFecha, 1, 3);
            addPane.add(newDescripcion, 1, 4);
            addPane.add(newCamara, 1, 5);
            addPane.add(newEtiqueta, 1, 6);
            
            Button examinar=new Button("Examinar");
            examinar.setOnAction((value2)->{
                
                FileChooser archivo=new FileChooser();
                File file=archivo.showOpenDialog(null);
                String foto=file.toURI().toString();
                Image image = new Image(foto);
                Imagen img=new Imagen(newNombre.getText(),newPersonas.getText(),newLugar.getText(),newFecha.getText(),newDescripcion.getText(),image,newCamara.getText(),newEtiqueta.getText(),newReaccion.getValue());
                for(Album token: albumes.keySet()){
                    if(token.getNombre()==albumSelect.getValue()){
                        actualizarLlave(token,img);
                        actualizar(albumSelect,desplazador,root);
                        
                    }
                }
                add.close();
            });
             
            Label addReaccion=new Label("Reaccion: ");
            
            addPane.add(addReaccion, 0,7);
            addPane.setHgap(10);
            addPane.setVgap(10);
            
            addPane.add(examinar,1,8);
            
            Scene addScene=new Scene(addPane,275,325);
            add.setScene(addScene);
            add.setTitle("Añadir imagen");
            add.show();
            });
        
        albumSelect.setOnAction((valorbox)->{
            actualizar(albumSelect,desplazador,root);
        
        });
        
        actualizar(albumSelect,desplazador,root);
        
        VBox cajaX=(VBox)desplazador.getContent();
        for (Node component : cajaX.getChildren()) {
            component.setOnMouseClicked((a)->{
                root.setCenter(component);
                ImageView ivAux=(ImageView)component;
                actualizar(albumSelect,desplazador,root);
                    
            });
        }

  
        Scene scene=new Scene(root,1000,600);
        stage.setTitle("galeria");
        stage.setScene(scene);
        stage.show();
    }
    private static synchronized void actualizarLlave(Album key,Imagen valor){
        ArrayList<Imagen> itemsList = albumes.get(key);
        itemsList.add(valor);
        albumes.put(key, itemsList);
    }
    
    private static void actualizar(ComboBox albumSelect,ScrollPane desplazador,BorderPane root){
            ArrayList<ImageView> list=new ArrayList();
            VBox cajaAux=new VBox(20);
            for(Album token:albumes.keySet()){
                if(token.getNombre()==albumSelect.getValue()){
                    for(Imagen img:albumes.get(token)){

                        ImageView aux=new ImageView(img.getImagen());
                        aux.setFitWidth(200);
                        aux.setFitHeight(150);
                        list.add(aux);
                        cajaAux.getChildren().add(aux);
                        for (Node component : cajaAux.getChildren()) {
                            component.setOnMouseClicked((a)->{
                                
                                ImageView ivAux=(ImageView)component;
                                ivAux.setFitHeight(300);
                                ivAux.setFitWidth(400);
                                root.setCenter(ivAux);
                                for(Album token2:albumes.keySet()){
                                    if(token.getNombre()==albumSelect.getValue()){
                                        for(Imagen img2:albumes.get(token2)){
                                            if(img2.getImagen().equals(ivAux.getImage())){
                                                GridPane caja2=new GridPane();
                                                caja2.setPadding(new Insets(10,10,10,10));
                                                caja2.add(new Label("Nombre: "), 0, 0);
                                                caja2.add(new Label(img2.getNombre()), 1, 0);
                                                caja2.add(new Label("Lugar: "), 0, 1);
                                                caja2.add(new Label(img2.getLugar()), 1, 1);
                                                caja2.add(new Label("Fecha: "), 0, 2);
                                                caja2.add(new Label(img2.getFecha()), 1, 2);
                                                caja2.add(new Label("Personas: "), 0, 3);
                                                caja2.add(new Label(img2.getPersonas()), 1, 3);
                                                caja2.add(new Label("Descripcion: "), 0, 4);
                                                caja2.add(new Label(img2.getDescripcion()), 1, 4);
                                                caja2.add(new Label("Etiqueta: "), 0, 5);
                                                caja2.add(new Label("#"+img2.getEtiqueta()), 1, 5);
                                                caja2.add(new Label("Camara: "), 0, 6);
                                                caja2.add(new Label(img2.getCamara()), 1, 6);
                                                caja2.add(new Label("Reaccion: "), 0, 7);
                                                caja2.add(new Label(img2.getReaccion()), 1, 7);
                                                root.setRight(caja2);
                                            }
                                    }}
                                }
                                        actualizar(albumSelect,desplazador,root);
                
            });
        }
                    }
                }
                desplazador.setContent(cajaAux);
            }
            

    }
    
    
    private static synchronized void albumDeMuestra(Map albumes,ComboBox caja){
        ArrayList<Imagen> muestras=new ArrayList<>();
        Album demo=new Album("Imagenes de muestra");
        Image imgLoader=new Image("file:carro.jpg");
        Imagen carro=new Imagen("carro","NA","NA","NA","Vehiculo ultimo modelo",imgLoader,"NA","NA","NA");
        muestras.add(carro);
        caja.getItems().add(demo.getNombre());
        caja.getSelectionModel().selectFirst();
        albumes.put(demo,muestras);
    }
    
    public synchronized static void guardar(ComboBox albumSelect,ScrollPane desplazador,BorderPane root){
        try {
            ObjectOutputStream escribir=new ObjectOutputStream(new FileOutputStream("C:\\Users\\Carlos\\Documents\\NetBeansProjects\\proyectofinal-poo\\MiGaleria\\file.txt"));
            escribir.writeObject(albumes);
            escribir.close();
            actualizar(albumSelect,desplazador,root);
        } catch (IOException ex) {
            actualizar(albumSelect,desplazador,root);
        }

    }
    
     public synchronized static void cargar(ComboBox albumSelect,ScrollPane desplazador,BorderPane root){
         
        try {
            ObjectInputStream leer = new ObjectInputStream(new FileInputStream("C:\\Users\\Carlos\\Documents\\NetBeansProjects\\proyectofinal-poo\\MiGaleria\\file.txt"));
            HashMap<Album,ArrayList<Imagen>> albumesRecuperados = (HashMap)leer.readObject();
            for(Album token:albumesRecuperados.keySet()){
                albumes.put(token, albumesRecuperados.get(token));
                System.out.println(token.getNombre());
                albumSelect.getItems().add(token.getNombre());
            }
            albumSelect.getSelectionModel().selectFirst();
            actualizar(albumSelect,desplazador,root);
        } catch (Exception ex) {
            albumDeMuestra(albumes,albumSelect);
            actualizar(albumSelect,desplazador,root);
        }
            
     }
     
    public void actualizarFiltro(ArrayList<Imagen> lista,ComboBox albumSelect,ScrollPane desplazador,BorderPane root){
        VBox cajaAux2=new VBox(20);
        for(Imagen img:lista){
            ImageView aux=new ImageView(img.getImagen());
            aux.setFitWidth(200);
            aux.setFitHeight(150);
            cajaAux2.getChildren().add(aux);
            for (Node component : cajaAux2.getChildren()) {
                            component.setOnMouseClicked((a)->{
                                
                                ImageView ivAux=(ImageView)component;
                                ivAux.setFitHeight(300);
                                ivAux.setFitWidth(400);
                                root.setCenter(ivAux);
                                for(Album token2:albumes.keySet()){
                                    if(token2.getNombre()==albumSelect.getValue()){
                                        for(Imagen img2:albumes.get(token2)){
                                            if(img2.getImagen().equals(ivAux.getImage())){
                                                GridPane caja2=new GridPane();
                                                caja2.setPadding(new Insets(10,10,10,10));
                                                caja2.add(new Label("Nombre: "), 0, 0);
                                                caja2.add(new Label(img2.getNombre()), 1, 0);
                                                caja2.add(new Label("Lugar: "), 0, 1);
                                                caja2.add(new Label(img2.getLugar()), 1, 1);
                                                caja2.add(new Label("Fecha: "), 0, 2);
                                                caja2.add(new Label(img2.getFecha()), 1, 2);
                                                caja2.add(new Label("Personas: "), 0, 3);
                                                caja2.add(new Label(img2.getPersonas()), 1, 3);
                                                caja2.add(new Label("Descripcion: "), 0, 4);
                                                caja2.add(new Label(img2.getDescripcion()), 1, 4);
                                                caja2.add(new Label("Etiqueta: "), 0, 5);
                                                caja2.add(new Label("#"+img2.getEtiqueta()), 1, 5);
                                                caja2.add(new Label("Camara: "), 0, 6);
                                                caja2.add(new Label(img2.getCamara()), 1, 6);
                                                caja2.add(new Label("Reaccion: "), 0, 7);
                                                caja2.add(new Label(img2.getReaccion()), 1, 7);
                                                root.setRight(caja2);
                                            }
                                    }}
                                }
                                        actualizarFiltro(lista,albumSelect,desplazador,root);
                
            });
        }
        }
            desplazador.setContent(cajaAux2);
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    
}
