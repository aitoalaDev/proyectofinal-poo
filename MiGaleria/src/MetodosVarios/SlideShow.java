/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MetodosVarios;

import entities.Album;
import entities.Imagen;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Municipio de Gye
 */
public class SlideShow implements Runnable{
    private HashMap<Album, ArrayList<Imagen>> albumes;
    
    public SlideShow(HashMap<Album, ArrayList<Imagen>> galeria, Album album) {
        ArrayList<Imagen> filImagen=new ArrayList<>(); 
        for (Imagen img: galeria.get(album)){
            filImagen.add(img);
        }
    
} 

    @Override
    public void run() {
            Stage mostrador=new Stage();
            BorderPane vistaImagen=new BorderPane();
            GridPane opciones=new GridPane();
            Button play=new Button("Play");
            Button stop=new Button("Stop");            
            Button siguiente=new Button("Siguiente ->");
            Button anterior=new Button("<- Anterior ");
            
          
            opciones.setHgap(4);
            opciones.add(anterior, 0, 0);
            opciones.add(play, 1, 0);
            opciones.add(stop, 2, 0);
            opciones.add(siguiente, 3, 0);
            vistaImagen.setBottom(opciones);
            Scene vistaGeneral=new Scene(vistaImagen,1000,700);
            mostrador.setScene(vistaGeneral);
            mostrador.setTitle("Vista en SlideShow");
            mostrador.show();    }
    
}
